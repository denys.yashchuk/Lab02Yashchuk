package ua.denysyashchuk.nc.DButils;

import oracle.jdbc.driver.OracleDriver;

import java.sql.*;

public class DBProcessor {

    private Connection connection;
    private final String URL = "jdbc:oracle:thin:@localhost:1521:xe";
    private final String USER_NAME = "meta";
    private final String PASSWORD = "1111";

    public DBProcessor() throws SQLException {
        DriverManager.registerDriver(new OracleDriver());
        connection = DriverManager.getConnection(URL, USER_NAME, PASSWORD);
    }

    public Connection getConnection() {
        return connection;
    }
}
